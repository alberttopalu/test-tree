<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class EmployeeResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request $request
   *
   * @return array
   */
  public function toArray($request)
  {
    return [
        'id' => $this->id,
        'name' => $this->name,
        'salary' => $this->salary,
        'date_employment' => $this->employment_date,
        'children' => boolval($this->subordinates->count()),
        'parent_name' => $this->chief->name ?? null,
        'position' => $this->position,
        'parent_id' => $this->parent_id,
        'photo_url' => $this->photo ? Storage::url($this->photo) : null
    ];
  }
}
