<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Head;
use App\Http\Resources\EmployeeResource;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;


class EmployeesController extends Controller
{

//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    public function index(Request $request)
    {

        $getAllEmployees = Employee::with('head','chief')->get()->toArray();

        return view('list', ['getAllEmployees' => $getAllEmployees]);
    }

    public function edit(Request $request){

        $getAllEmployees = Employee::with('head','chief')->get()->toArray();

        $getEmployeesParent = DB::table('employees')
            ->groupBy('id', 'parent_id')
              ->get();

        $getEmployeesParent = json_decode($getEmployeesParent, true);

        $getAllHeads = Head::all()->toArray();
         return view('list-update', [
            'getAllEmployees' => $getAllEmployees,
            'getAllHeads' => $getAllHeads,
            'getEmployeesParent' => $getEmployeesParent,
         ]);
    }

    public function update(Request $request)
    {
        $getAllEmployees = Employee::find($request['id']);

        $image = $request->file('photo');
        if (!empty($image)){
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/storage');
            $image->move($destinationPath, $name);

            $pathDb = '/storage/'.$name;
        }else{
            $pathDb = $getAllEmployees['img'];
        }

        DB::table('employees')
            ->where('id', $getAllEmployees['id'])
            ->update([
                'name' => $request['name'],
                'salary' => $request['salary'],
                'head_id' => $request['position'],
                'parent_id' => $request['chief'],
                'date_employment' => $request['date_employment'],
                 'img' => $pathDb
            ]);
        return redirect()->back();
    }

    public function delete( Request $request){
        Employee::where('id', $request['id'])->delete();
        return redirect()->back();

    }
}
