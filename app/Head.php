<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Head extends Model
{
    protected $fillable = [
        'name'
    ];

    public function employees()
    {
        return $this->belongsTo(Employee::class, 'head_id');
    }
}
