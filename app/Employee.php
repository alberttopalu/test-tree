<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Employee extends Model
{


    protected $fillable
        = [
            'name',
            'parent_id',
            'head_id',
            'date_employment',
            'img',
            'salary',
        ];

    public function position()
    {
        return $this->hasOne('App\Head', 'id', 'head_id');
    }

    public function chief()
    {
        return $this->belongsTo('App\Employee', 'parent_id');
    }

    public function subordinates()
    {
        return $this->hasMany('App\Employee', 'parent_id');
    }




    public function parent()
    {
        return $this->belongsTo('App\Employee', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Employee', 'parent_id');
    }

    public function replies()
    {
        return $this->hasMany(Employee::class,'parent_id','id');
    }

    public function head(){
        return $this->hasOne(Head::class,'id','head_id');

    }
}
