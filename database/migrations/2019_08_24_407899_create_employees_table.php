<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->timestamps();
        });

        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('head_id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->foreign('head_id')->references('id')->on('heads');
            $table->foreign('parent_id')->references('id')->on('employees');
            $table->string('name', 100);
             $table->string('img', 150)->nullable();
            $table->decimal('salary', 9, 2);
             $table->date('date_employment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
        Schema::dropIfExists('heads');
    }
}
