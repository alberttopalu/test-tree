<?php

use App\Employee;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeesTableSeeder extends Seeder
{
  public function __construct()
  {
    $this->faker = Faker\Factory::create();
  }

  public function run()
  {


    ini_set('memory_limit', '2048M');
    DB::connection()->disableQueryLog();
    $start = microtime(true);

    factory(Employee::class, 5)->create(['head_id' => 1])->each(function ($q) {
      factory(Employee::class, 10)->create(['parent_id' => $q->id, 'head_id' => 2])->each(function ($w) {
        factory(Employee::class, 10)->create(['parent_id' => $w->id, 'head_id' => 3])->each(function ($e) {
          factory(Employee::class, 10)->create(['parent_id' => $e->id, 'head_id' => 4])->each(function ($r) {
            factory(Employee::class, 10)->create(['parent_id' => $r->id, 'head_id' => 5])->each(function ($t) {
              $this->command->info($t->id);
            });
          });
        });
      });
    });

    $time = microtime(true) - $start;
    $this->command->info($time);
  }
}
