<?php

use Illuminate\Database\Seeder;

class HeadsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        \Illuminate\Support\Facades\DB::table('heads')->insert([
            ['name' => 'head'],
            ['name' => 'chief'],
            ['name' => 'leader'],
            ['name' => 'manager'],
            ['name' => 'user'],
        ]);
    }
}
