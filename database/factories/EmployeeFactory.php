<?php

use Faker\Generator as Faker;

$factory->define(App\Employee::class, function (Faker $faker) {
  return [
      'name' => $this->faker->name,
      'head_id' => \App\Head::first()->id,
      'date_employment' => $this->faker->date('Y-m-d'),
      'salary' => $this->faker->randomFloat(3, 500, 500),
  ];
});