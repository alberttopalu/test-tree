@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="container">
            {{--@guest--}}
            <table class="table responsive" id="sort"  style="min-width: 960px">
                <thead>
                <tr>
                    <th  class="col-md-1" scope="col">id</th>
                    <th class="col-md-2" scope="col">Name</th>
                    <th class="col-md-1" scope="col">Salary</th>
                    <th  class="col-md-2" scope="col">Position</th>
                    <th  class="col-md-1" scope="col">Chief</th>
                    <th  class="col-md-2" scope="col">Date</th>
                    <th  class="col-md-1" scope="col">Img</th>
                </tr>
                </thead>

                <tbody>

                @foreach($getAllEmployees as $employee)
                <tr>
                    <td class="col-md-1" data-table-header="id">{{$employee['id']}} </td>
                    <td class="col-md-2" data-table-header="Name">{{$employee['name']}}</td>
                    <td class="col-md-2" data-table-header="Salary">{{$employee['salary']}}</td>
                     <td class="col-md-2" data-table-header="Position">{{$employee['head']['name']}}</td>
                     <td class="col-md-2" data-table-header="Chief">{{$employee['chief']['name']}}</td>
                    <td  class="col-md-2" data-table-header="Date">{{$employee['date_employment']}}</td>
                    <td class="col-md-1" data-table-header="Img">

                        <img src="{{ \Illuminate\Support\Facades\URL::asset($employee['img']) }}" height="100" width="100" style="float: left" />

                    </td>
                </tr>
                    @endforeach

                </tbody>
            </table>

        </div>
    </div>

@endsection