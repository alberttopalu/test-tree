@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="container">
            <table class="table responsive" id="sort" style="min-width: 960px">
                <thead>
                <tr>
                    <th class="col-md-1" scope="col">id</th>
                    <th class="col-md-2" scope="col">Name</th>
                    <th class="col-md-1" scope="col">Salary</th>
                    <th class="col-md-1" scope="col">Position</th>
                    <th  class="col-md-1" scope="col">Chief</th>
                    <th class="col-md-1" scope="col">Date</th>
                    <th class="col-md-1" scope="col">Img</th>
                    <th class="col-md-2" scope="col">Actions</th>
                </tr>
                </thead>

                <tbody>

                @foreach($getAllEmployees as $employee)

                    <tr>
                        <td class="col-md-1" data-table-header="id">{{$employee['id']}} </td>
                        <td class="col-md-2" data-table-header="Name">{{$employee['name']}}</td>
                        <td class="col-md-2" data-table-header="Salary">{{$employee['salary']}}</td>
                        <td class="col-md-2" data-table-header="Position">{{$employee['head']['name']}}</td>
                        <td class="col-md-2" data-table-header="Chief">{{$employee['chief']['name']}}</td>

                        <td class="col-md-2" data-table-header="Date">{{$employee['date_employment']}}</td>
                        <td class="col-md-1" data-table-header="Img">

                             <img src="{{ \Illuminate\Support\Facades\URL::asset($employee['img']) }}" height="100" width="100" style="float: left" />

                        </td>
                        <td class="col-md-2" data-table-header="Actions">
                             <button type="button" class="btn btn-info btn-lg" data-toggle="modal"
                                    data-target="#EditModal{{$employee['id']}}">Edit
                            </button>

                            <a href="{{route('employees-delete', $employee['id'])}}">
                                <button type="button" class="btn btn-danger btn-lg" data-toggle="modal"
                                        data-target="#DeleteModal{{$employee['id']}}">Delete
                                </button>
                            </a>
                        </td>
                    </tr>

                    {{---------------------modal update---------------}}


                    <div class="modal" id="EditModal{{$employee['id']}}" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Edit Information</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="{{ url('/list-update', $employee['id'])}}" method="POST" enctype="multipart/form-data"
                                          name="modal.{{$employee['id']}}.">
                                        {{ csrf_field() }}
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-3 col-form-label">Name</label>
                                            <div class="col-sm-9">
                                                <input name="name" type="text" value="{{$employee['name']}}"
                                                       class="form-control">
                                                <span class="invalid-feedback">
                                                    <strong></strong>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="salary" class="col-sm-3 col-form-label">Salary</label>
                                            <div class="col-sm-9">
                                                <input name="salary" value="{{$employee['salary']}}" type="text"
                                                       class="form-control">
                                                <span class="invalid-feedback">
                                                        <strong></strong>
                                                    </span>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="employment_date" class="col-sm-3 col-form-label">Employment
                                                Date</label>
                                            <div class="col-sm-9">
                                                <input name="date_employment" value="{{$employee['date_employment']}}"
                                                       type="date" class="form-control">
                                                <span class="invalid-feedback">
                                                        <strong></strong>
                                                    </span>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="position" class="col-sm-3 col-form-label">Select  position:</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="position" style="height:2.4em;">
                                                    @foreach ($getAllHeads as $item)
                                                        @if( $item['id'] == $employee['head_id'] )
                                                            <option value="{{ $item['id'] }}"
                                                                    selected="selected"> {{ $item['name'] }}</option>
                                                        @else
                                                            <option value="{{ $item['id'] }}"> {{ $item['name'] }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="chief" class="col-sm-3 col-form-label">Select  chief:</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="chief" style="height:2.4em;">
                                                    @foreach ($getEmployeesParent as $chief)
                                                        @if( $chief['id'] == $employee['parent_id'] )
                                                            <option value="{{ $chief['id'] }}"
                                                                    selected="selected"> {{ $chief['name'] }}</option>
                                                        @else
                                                            <option value="{{ $chief['id'] }}"> {{ $chief['name'] }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="profile_image" class="col-sm-3 col-form-label text-md-right">Profile Image</label>
                                            <div class="col-sm-9">
                                                <input id="photo" type="file" class="form-control" name="photo">

                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" id="submit">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


                    {{-----------------END modal update---------------}}


                @endforeach


                </tbody>
            </table>
        </div>
    </div>

@endsection