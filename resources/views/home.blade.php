@extends('layouts.app')

@section('content')

    <style>
    ul, #myUL {
    list-style-type: none;
    }

    #myUL {
    margin: 0;
    padding: 0;
    }

    .box {
    cursor: pointer;
    -webkit-user-select: none; /* Safari 3.1+ */
    -moz-user-select: none; /* Firefox 2+ */
    -ms-user-select: none; /* IE 10+ */
    user-select: none;
    }

    .box::before {
    content: "\2610";
    color: black;
    display: inline-block;
    margin-right: 6px;
    }

    .check-box::before {
    content: "\2611";
    color: dodgerblue;
    }

    .nested {
    display: none;
    }

    .active {
    display: block;
    }
    </style>
                <div class="container">
    <ul id="myUL">

        @foreach($cool as $cool1)
            <li><span class="box">{{$cool1['name']  }} </span>
                <ul class="nested">

                    @foreach($cool1['children'] as $cool2)
                        <li><span class="box">{{$cool2['name']}}</span>

                            <ul class="nested">
                                @foreach($cool2['subordinates'] as $cool3)



                                    <li><span class="box">{{$cool3['name']}}</span>

                                        <ul class="nested">

                                                <li><span class="box">{{$cool3['parent']['name']}}</span></li>
                                         </ul>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    @endforeach
                </ul>
            </li>
        @endforeach
    </ul>
    </div>

<script>
    var toggler = document.getElementsByClassName("box");
    var i;

    for (i = 0; i < toggler.length; i++) {
        toggler[i].addEventListener("click", function() {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("check-box");
        });
    }
</script>



@endsection